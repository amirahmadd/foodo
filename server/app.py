from flask import Flask, jsonify, request, abort, Response, send_file, render_template
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_jwt_extended import create_access_token
from flask_mongoengine import MongoEngine
from flask_jwt_extended import JWTManager
from flask_bcrypt import generate_password_hash, check_password_hash
from flask_jwt_extended import jwt_required, get_jwt_identity
from werkzeug.utils import redirect, secure_filename
from flask_cors import CORS
import datetime
import json
import os

UPLOAD_DIR = './media'
CLIENT_URL = 'http://localhost:3000'
TEXTS = {
    'en': {
        'title': 'sing up',
        'username': 'USERNAME',
        'password': 'PASSWORD',
        'repeat': 'REPEAT PASSWORD',
        'email': 'EMAIL',
        'singup': 'SIGN UP',
        'alreadyMember': "Already Member?"
    },
    'fa': {
        'title': 'ثبت نام',
        'username': 'نام کاربری',
        'password': 'رمز عبور',
        'repeat': 'تکرار رمز عبور',
        'email': 'ایمیل',
        'singup': 'ثبت نام',
        'alreadyMember': "!حساب کاربری دارم"
    }
}
ERRORS_TEXT = {
    'en': {
        'username': 'Duplicate username',
        'password': 'Enter the password carefully',
        'empty': 'Fill in all the fields',
    },
    'fa': {
        'username': 'نام کاربری تکراری است',
        'password': 'رمز عبور را با دقت وارد کنید',
        'empty': 'تمام فیلد ها را پر کنید',
    }
}
# app config
app = Flask(__name__)
app.config['SECRET_KEY'] = 'IE_PROJECT'
app.secret_key = 'IE_PROJECT'
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app, resources={r"/*": {"origins": "*"}})
# app.config.from_envvar('ENV_FILE_LOCATION')
app.config['JWT_SECRET_KEY'] = 't1NP63m4wnBg6nyHYKfmc2TpCOGI4nss'
app.config['UPLOAD_DIR'] = UPLOAD_DIR
jwt = JWTManager(app)

# db config
app.config['MONGODB_SETTINGS'] = {
    'db': 'IE_project',
    'host': 'localhost',
    'port': 27017
}
db = MongoEngine()
db.init_app(app)

def Reverse(lst):
    lst.reverse()
    return lst

class User(db.Document):
    userName = db.StringField(unique=True)
    email = db.StringField()
    password = db.StringField()
    rooms = db.ListField(db.StringField(), default=[])
    bio = db.StringField()
    avatar = db.StringField()
    background = db.StringField()

    def get_user(self):
        return {
            'userName': self.userName,
            'email': self.email,
            'rooms': Reverse(self.rooms),
            'bio': self.bio,
            'avatar': self.avatar,
            'background': self.background,
            'id': str(self.id)
        }

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf8')

    def check_password(self, password):
        return check_password_hash(self.password, password)


class Message(db.Document):
    text = db.StringField()
    time = db.StringField()
    sender = db.ReferenceField(User)
    isSeen = db.IntField(default=0)
    userName = db.StringField()


class Room(db.Document):
    members = db.ListField(db.ReferenceField(User), default=[])
    messages = db.ListField(db.StringField(), default=[])
    group = db.IntField(default=0)
    title = db.StringField()
    bio = db.StringField()
    avatar = db.StringField()


# socket config
socket = SocketIO(app, cors_allowed_origins="*")
app.host = 'localhost'


@socket.on("connect")
def handleConnection():
    print('connect')


@socket.on("joinRoom")
def handleJoinRoom(roomId):
    join_room(roomId)
    print('joined to self room')


@socket.on("joinChat")
def handleJoinChat(roomId):
    try:
        join_room(roomId)
        print('joined to chat')
        room = Room.objects(id=roomId).first()
        emit('updateMessages', room.messages)
    except Exception as e:
        abort(400)


@socket.on("leaveChat")
def handleLeaveRoom(roomId):
    leave_room(roomId)
    print('leave room')


@socket.on("message")
def handleMessage(data):
    try:
        message = Message(
            text=data['msg']['text'], time=data['msg']['time'], sender=data['msg']['sender'])
        message.save()
        emit("newMessage", str(message.id), to=data['room'])
        room = Room.objects(id=data['room']).first()
        room.messages.append(str(message.id))
        room.save()
        for user in room.members:
            emit("updateRooms", to=str(user.id))
            if(message.sender != user):
                emit("msgnotification",
                     message.sender.userName, to=str(user.id))

    except Exception as e:
        print(e)


@socket.on("istyping")
def handleistyping(data):
    emit('istyping', data['userId'], to=data['room'])
    print('istyping')


@socket.on("endtyping")
def handleendtyping(data):
    emit('endtyping', data['userId'], to=data['room'])
    print('endtyping')


@app.route('/rooms/<roomId>', methods=['POST'])
@jwt_required()
def roomInfo(roomId):
    try:
        data = json.loads(request.data)
        room = Room.objects(id=roomId).first()
        user = User.objects(userName=data['userName']).first()
        index = user.rooms.index(roomId)
        info = {"members": [], "lastMessage": {},
                "title": room.title, "bio": room.bio, "avatar": room.avatar}

        if(len(room.messages) > 0):
            lastMessage = Message.objects(id=room.messages[-1]).first()
            info["lastMessage"] = {
                "text": lastMessage.text, "time": lastMessage.time, "isSeen": lastMessage.isSeen, "sender": str(lastMessage.sender.id)}
        for user in room.members:
            info["members"].append(
                {"userName": user.userName, "avatar": user.avatar, "bio": user.bio})
        return jsonify(info)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/message/<msgId>', methods=['POST'])
@jwt_required()
def messageInfo(msgId):
    try:
        data = json.loads(request.data)
        message = Message.objects(id=msgId).first()
        if(data['userId'] != str(message.sender.id)):
            message.isSeen = 1
            message.save()
            socket.emit("updateRooms", to=data['userId'])
            socket.emit("updateRooms", to=str(message.sender.id))
        message.userName = message.sender.userName
        return jsonify(message)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/signup/<lang>', methods=['GET', 'POST'])
def signup(lang):
    if(request.method == 'GET'):
        return render_template('signup.html', client=CLIENT_URL, texts=TEXTS[lang])
    else:
        try:
            if(request.form['username'] != "" and request.form['email'] != "" and request.form['password'] != ""):
                if(request.form['password'] == request.form['repass']):
                    user = User(userName=request.form['username'],
                                email=request.form['email'], password=request.form['password'])
                    user.hash_password()
                    user.save()
                    return redirect(CLIENT_URL)
                else:
                    return render_template('signup.html', client=CLIENT_URL, texts=TEXTS[lang], error=ERRORS_TEXT[lang]['password'])
            else:
                return render_template('signup.html', client=CLIENT_URL, texts=TEXTS[lang], error=ERRORS_TEXT[lang]['empty'])
        except Exception as e:
            print(e)
            return render_template('signup.html', client=CLIENT_URL, texts=TEXTS[lang], error=ERRORS_TEXT[lang]['username'])


@app.route('/login', methods=['POST'])
def login():
    data = json.loads(request.data)
    try:
        user = User.objects(userName=data['userName']).first()
        authorized = user.check_password(data['password'])
        if not authorized:
            abort(403)
        expires = datetime.timedelta(days=7)
        access_token = create_access_token(
            identity=str(user.id), expires_delta=expires)
        return {'token': access_token, "user": user.get_user()}, 200

    except Exception as e:
        print(e)
        abort(500)


@app.route('/current')
@jwt_required()
def currentUser():
    try:
        user_id = get_jwt_identity()
        user = User.objects(id=user_id).first()
        return jsonify(user.get_user())
    except Exception as e:
        # print(e)
        abort(500)


@app.route('/search', methods=['POST'])
@jwt_required()
def search():
    data = json.loads(request.data)
    try:
        user = User.objects(userName=data['userName']).first()
        owner = User.objects(id=data['id']).first()
        if(user.id != owner.id):
            newRoom = Room(members=[owner.id, user.id])
            newRoom.save()
            user.rooms.append(str(newRoom.id))
            owner.rooms.append(str(newRoom.id))
            user.save()
            owner.save()
            socket.emit("newRoom", str(newRoom.id), to=str(user.id))
            return jsonify(str(newRoom.id))
        else:
            abort(400)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/group/new', methods=['POST'])
@jwt_required()
def newGroup():
    data = json.loads(request.data)
    try:
        owner = User.objects(id=data['id']).first()
        newRoom = Room(members=[owner.id], group=1, title=data['title'])
        newRoom.save()
        owner.rooms.append(str(newRoom.id))
        owner.save()
        # socket.emit("newRoom", str(newRoom.id), to=str(owner.id))
        return jsonify(str(newRoom.id))
    except Exception as e:
        print(e)
        abort(400)


@app.route('/group/add_member', methods=['POST'])
@jwt_required()
def addMember():
    # TODO add member to group by userName
    data = json.loads(request.data)
    try:
        group = Room.objects(id=data['roomId']).first()
        user = User.objects(userName=data['userName']).first()
        if(user.id in group.members):
            abort(401)
        user.rooms.append(str(group.id))
        group.members.append(user.id)
        user.save()
        group.save()
        socket.emit("updateRooms", to=data['roomId'])
        socket.emit("updateRooms", to=str(user.id))
        return jsonify(group)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/group/remove_member', methods=['POST'])
@jwt_required()
def removeMember():
    data = json.loads(request.data)
    try:
        group = Room.objects(id=data['roomId']).first()
        user = User.objects(userName=data['userName']).first()
        user.rooms.remove(str(group.id))
        group.members.remove(user)
        user.save()
        group.save()
        socket.emit("removed", user.userName, to=data['roomId'])
        socket.emit("updateRooms", to=data['roomId'])
        socket.emit("updateRooms", to=str(user.id))
        return jsonify(group)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/group/edit', methods=['POST'])
@jwt_required()
def editGroup():
    data = json.loads(request.data)
    try:
        group = Room.objects(id=data['roomId']).first()
        group.bio = data['bio']
        group.title = data['title']
        group.save()
        socket.emit("updateRooms", to=data['roomId'])
        return jsonify(group)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/user/edit', methods=['POST'])
@jwt_required()
def editUser():
    data = json.loads(request.data)
    try:
        user = User.objects(userName=data['userName']).first()
        user.bio = data['bio']
        user.save()
        socket.emit("updateRooms", to=str(user.id))
        return jsonify(user.get_user())
    except Exception as e:
        print(e)
        abort(400)


@app.route('/user/avatar', methods=['POST', 'DELETE'])
@jwt_required()
def editUserAvatar():
    try:
        user_id = get_jwt_identity()
        user = User.objects(id=user_id).first()
        if(request.method == 'POST'):
            avatar = request.files['avatar']
            if(avatar.filename == ''):
                abort(400)
            filename = user_id + "_" + secure_filename(avatar.filename)
            dir = app.config['UPLOAD_DIR'] + "/avatar/" + filename
            user.avatar = filename
            user.save()
            avatar.save(dir)
            return jsonify(user.get_user())
        else:
            user.avatar = ""
            user.save()
            return jsonify(user.get_user())
    except Exception as e:
        print(e)
        abort(400)


@app.route('/user/background', methods=['POST', 'DELETE'])
@jwt_required()
def editUserbackground():
    try:
        user_id = get_jwt_identity()
        user = User.objects(id=user_id).first()
        if(request.method == 'POST'):
            image = request.files['image']
            if(image.filename == ''):
                abort(400)
            filename = user_id + "_" + secure_filename(image.filename)
            dir = app.config['UPLOAD_DIR'] + "/bg/" + filename
            user.background = filename
            user.save()
            image.save(dir)
            return jsonify(user.get_user())
        else:
            user.background = ""
            user.save()
            return jsonify(user.get_user())
    except Exception as e:
        print(e)
        abort(400)


@app.route('/group/avatar/<roomId>', methods=['POST', 'DELETE'])
@jwt_required()
def editGroupAvatar(roomId):
    try:
        user_id = get_jwt_identity()
        room = Room.objects(id=roomId).first()
        if(str(room.members[0].id) == user_id):
            if(request.method == 'POST'):
                avatar = request.files['avatar']
                if(avatar.filename == ''):
                    abort(400)
                filename = roomId + "_" + secure_filename(avatar.filename)
                dir = app.config['UPLOAD_DIR'] + "/avatar/" + filename
                room.avatar = filename
                room.save()
                avatar.save(dir)
                socket.emit("updateRooms", to=roomId)
                return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
            else:
                room.avatar = ""
                room.save()
                socket.emit("updateRooms", to=roomId)
                return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}

        else:
            abort(403)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/media/avatar/<fileName>')
def getAvatar(fileName):
    try:
        avatar_dir = app.config['UPLOAD_DIR'] + "/avatar/"
        img_path = os.path.join(avatar_dir, fileName)
        return send_file(img_path, mimetype='image/*')
    except Exception as e:
        abort(400)


@app.route('/media/bg/<fileName>')
def getBackground(fileName):
    try:
        bg_dir = app.config['UPLOAD_DIR'] + "/bg/"
        img_path = os.path.join(bg_dir, fileName)
        return send_file(img_path, mimetype='image/*')
    except Exception as e:
        abort(400)


@app.route('/chat/leave', methods=['POST'])
@jwt_required()
def leaveChat():
    data = json.loads(request.data)
    try:
        group = Room.objects(id=data['roomId']).first()
        user = User.objects(userName=data['userName']).first()
        if(user.id in group.members):
            abort(401)
        user.rooms.remove(str(group.id))
        group.members.remove(user)
        user.save()
        group.save()
        socket.emit("left", user.userName, to=data['roomId'])
        socket.emit("updateRooms", to=data['roomId'])
        socket.emit("updateRooms", to=str(user.id))
        return jsonify(group)
    except Exception as e:
        print(e)
        abort(400)


@app.route('/')
def index():
    return 'leave me alone and visit our client , this is api server'


@app.route('/logout', methods=['POST'])
@jwt_required()
def logout():
    data = json.loads(request.data)
    # if data['session'] in session:
    #     session.pop(data['username'], None)
    abort(200)


if __name__ == '__main__':
    app.run(debug=True)
