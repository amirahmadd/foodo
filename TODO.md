# Foodo

## real time Chat Room

IE project

### common

- [x] init project
- [x] git repo
- [x] Specify features

### server

- [x] init basic server
- [x] virtualenv
- [x] add database package for flask
- [x] database models
- [x] file structure
- [x] basic api for models (find , update)
- [x] create socket
- [x] handle socket connection
- [x] handle rooms
- [x] handle new message
- [ ] signup
- [x] auth
- [x] istyping event
- [x] update rooms
- [x] user model
- [x] rooms model
- [x] messages model
- [x] edit groups
- [x] edit users
- [x] add group member
- [x] add user avatar
- [x] add group avatar
- [x] add group bio
- [x] add user bio
- [x] bio update problem
- [x] change background (by user)
- [x] add background in user model
- [x] add image dir for bgs
- [ ] refactor app.py

### client

- [x] init client side
- [x] add required packages
- [x] file structure
- [x] UI/UX design
- [x] rtl theme
- [x] toastify feature
- [x] redux action/reducers structure
- [x] api manager basis & configuration
- [x] login page
- [x] chat room layout
- [x] create socket
- [x] handle rooms list
- [x] handle room messages
- [x] send message
- [x] join room
- [x] handle auth
- [x] edit user profile
- [x] left room
- [x] handle is typing
- [x] group info
- [x] user profile
- [x] handle user avatar
- [x] handle group avatar
- [x] new message notification
- [x] user & group edit box
- [x] add member to group
- [x] remove member
- [x] background color
- [x] bio update problem
- [x] manual scroll
- [x] change language
- [x] add languages list
- [x] add dict
- [x] change direction on language
- [x] load lang from local storage
- [x] change background (by user)
- [x] singup page (jinja)
