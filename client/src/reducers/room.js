import {
  SET_SOCKET,
  SET_ROOMS,
  SET_CURRENT_ROOM,
  SET_MESSAGES,
  SET_NEW_MESSAGE,
  SET_IS_TYPING,
  SET_UPDATE_ROOM_STATUS,
  SET_GROUPS,
  SET_SOCKET_STATUS,
  SET_LANGUAGE,
} from "actions";

const initialState = {
  socket: null,
  isTyping: false,
  notification: true,
  socketStatus: false,
  language: window.localStorage.getItem("__IE_PROJECT_lang__") || "fa",
  updateRoomStatus: 0,
  rooms: [],
  groups: [],
  currentRoom: {},
  messages: [],
};

const roomReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SOCKET:
      return {
        ...state,
        socket: action.payload,
      };
    case SET_ROOMS:
      return {
        ...state,
        rooms: action.payload,
      };
    case SET_CURRENT_ROOM:
      return {
        ...state,
        currentRoom: action.payload,
      };
    case SET_MESSAGES:
      return {
        ...state,
        messages: action.payload,
      };
    case SET_NEW_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.payload],
      };
    case SET_IS_TYPING:
      return {
        ...state,
        isTyping: action.payload,
      };
    case SET_UPDATE_ROOM_STATUS:
      return {
        ...state,
        updateRoomStatus: state.updateRoomStatus + 1,
      };
    case SET_GROUPS:
      return {
        ...state,
        groups: action.payload,
      };
    case SET_SOCKET_STATUS:
      return {
        ...state,
        socketStatus: action.payload,
      };
    case SET_LANGUAGE:
      return {
        ...state,
        language: action.payload,
      };
    default:
      return state;
  }
};

export default roomReducer;
