import { combineReducers } from "redux";
import authReducer from "./auth";
import roomReducer from "./room";

const rootReducer = combineReducers({
  auth: authReducer,
  room: roomReducer,
});

export default rootReducer;
