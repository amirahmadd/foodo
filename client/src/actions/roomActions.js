import {
  SET_SOCKET,
  SET_ROOMS,
  SET_CURRENT_ROOM,
  SET_MESSAGES,
  SET_NEW_MESSAGE,
  SET_IS_TYPING,
  SET_UPDATE_ROOM_STATUS,
  SET_GROUPS,
  SET_NOTIFICATION_STATUS,
  SET_LANGUAGE,
  SET_SOCKET_STATUS,
} from "./actionTypes";

export const setSocket = (socket) => {
  return {
    type: SET_SOCKET,
    payload: socket,
  };
};

export const setRooms = (rooms) => {
  return {
    type: SET_ROOMS,
    payload: rooms,
  };
};

export const setCurrentRoom = (room) => {
  return {
    type: SET_CURRENT_ROOM,
    payload: room,
  };
};

export const setMessages = (messages) => {
  return {
    type: SET_MESSAGES,
    payload: messages,
  };
};

export const setNewMessage = (message) => {
  return {
    type: SET_NEW_MESSAGE,
    payload: message,
  };
};

export const setIsTyping = (status) => {
  return {
    type: SET_IS_TYPING,
    payload: status,
  };
};

export const setUpdateRoomStatus = () => {
  return {
    type: SET_UPDATE_ROOM_STATUS,
  };
};

export const setGroups = () => {
  return {
    type: SET_GROUPS,
  };
};

export const setNotificationStatus = () => {
  return {
    type: SET_NOTIFICATION_STATUS,
  };
};

export const setLanguage = (lang) => {
  return {
    type: SET_LANGUAGE,
    payload: lang,
  };
};

export const setSocketStatus = (status) => {
  return {
    type: SET_SOCKET_STATUS,
    payload: status,
  };
};
