import React, { Suspense } from "react";
import { StylesProvider, ThemeProvider } from "@material-ui/core";
import { theme, jss } from "theme";
import { ToastContainer } from "react-toastify";
import { reducers } from "reducers";
import { createStore } from "redux";
import { Provider } from "react-redux";
import Routes from "Routes";
import { Spinner } from "components";

import "react-toastify/dist/ReactToastify.css";
import "./App.css";

const App = () => {
  const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
  return (
    <Suspense fallback={<Spinner />}>
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Provider store={store}>
            <Routes />
          </Provider>
          <ToastContainer rtl={true} pauseOnFocusLoss={false} draggable />
        </ThemeProvider>
      </StylesProvider>
    </Suspense>
  );
};

export default App;
