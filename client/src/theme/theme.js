/* eslint-disable eqeqeq */
import { createMuiTheme, jssPreset } from "@material-ui/core";
import { create } from "jss";
import rtl from "jss-rtl";

export const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
export const theme = createMuiTheme({
  direction:
    window.localStorage.getItem("__IE_PROJECT_lang__") == "fa" ||
    window.localStorage.getItem("__IE_PROJECT_lang__") == null
      ? "rtl"
      : "ltr",
  spacing: 5,
  palette: {
    primary: {
      main: "#5a8d9d",
    },
  },
  typography: {
    fontFamily: [
      "IRANYekan",
      '"IRANYekan"',
      "B Yekan",
      '"B Yekan"',
      "Yekan",
      '"Yekan"',
      "tahoma",
    ].join(","),
  },
  overrides: {
    MuiFilledInput: {
      root: {
        backgroundColor: "#fafafa",
        borderRadius: "10px",
        "&$focused": {
          backgroundColor: "#fafafa",
        },
      },
    },
  },
});
