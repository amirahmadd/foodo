import API from "./config";
import * as URI from "./URI";

class apiCallManager {
  login(data) {
    return API.post(URI.LOGIN, data, {
      headers: {
        "Content-Type": "application/json",
      },
    });
  }
  logout() {
    return API.post(
      URI.LOGOUT,
      { session: window.localStorage.getItem("__IE_PROJECT__") },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${window.localStorage.getItem(
            "__IE_PROJECT__"
          )}`,
        },
      }
    );
  }
  currentUser() {
    return API.get(URI.CURRENT_USER, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  search(data) {
    return API.post(
      URI.USER_SEARCH,
      { ...data, session: window.localStorage.getItem("__IE_PROJECT__") },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${window.localStorage.getItem(
            "__IE_PROJECT__"
          )}`,
        },
      }
    );
  }
  getRoomInfo(id, userName) {
    return API.post(
      URI.ROOM_INFO.replace(":id", id),
      { userName },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${window.localStorage.getItem(
            "__IE_PROJECT__"
          )}`,
        },
      }
    );
  }
  getMessages(roomId) {
    return API.get(URI.ROOM_MESSAGES.replace(":id", roomId), {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  getOneMessage(msgId, userId) {
    return API.post(
      URI.MESSAGE.replace(":id", msgId),
      { userId },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${window.localStorage.getItem(
            "__IE_PROJECT__"
          )}`,
        },
      }
    );
  }
  newGroup(data) {
    return API.post(URI.NEW_GROUP, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  addMember(data) {
    return API.post(URI.ADD_MEMBER, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  editGroup(data) {
    return API.post(URI.EDIT_GROUP, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  leaveChat(data) {
    return API.post(URI.LEAVE_CHAT, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  removeMember(data) {
    return API.post(URI.REMOVE_MEMBER, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  editUser(data) {
    return API.post(URI.EDIT_USER, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  editUserAvatar(avatar) {
    const data = new FormData();
    data.append("avatar", avatar);
    return API.post(URI.EDIT_USER_AVATAR, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  editGroupAvatar(avatar, roomId) {
    const data = new FormData();
    data.append("avatar", avatar);
    return API.post(URI.EDIT_GROUP_AVATAR.replace(":roomId", roomId), data, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  removeUserAvatar() {
    return API.delete(URI.EDIT_USER_AVATAR, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  removeGroupAvatar(roomId) {
    return API.delete(URI.EDIT_GROUP_AVATAR.replace(":roomId", roomId), {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  editUserBackground(image) {
    const data = new FormData();
    data.append("image", image);
    return API.post(URI.EDIT_USER_BACKGROUND, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
  removeUserBackground() {
    return API.delete(URI.EDIT_USER_BACKGROUND, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${window.localStorage.getItem(
          "__IE_PROJECT__"
        )}`,
      },
    });
  }
}

const api = new apiCallManager();

export default api;
