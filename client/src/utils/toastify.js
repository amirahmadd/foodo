/* eslint-disable eqeqeq */
import { toast } from "react-toastify";

export const toastSuccess = (message) => {
  toast.success(message, {
    position:
      window.localStorage.getItem("__IE_PROJECT_lang__") == "fa" ||
      window.localStorage.getItem("__IE_PROJECT_lang__") == null
        ? toast.POSITION.TOP_LEFT
        : toast.POSITION.TOP_RIGHT,
  });
};

export const toastError = (error, text) => {
  if (text) {
    toast.error(error, {
      position:
        window.localStorage.getItem("__IE_PROJECT_lang__") == "fa" ||
        window.localStorage.getItem("__IE_PROJECT_lang__") == null
          ? toast.POSITION.TOP_LEFT
          : toast.POSITION.TOP_RIGHT,
    });
  } else {
    toast.error(`${error.response?.status}`, {
      position:
        window.localStorage.getItem("__IE_PROJECT_lang__") == "fa" ||
        window.localStorage.getItem("__IE_PROJECT_lang__") == null
          ? toast.POSITION.TOP_LEFT
          : toast.POSITION.TOP_RIGHT,
    });
  }
};

export const toastWarning = (message) => {
  toast.dark(message, {
    position: toast.POSITION.TOP_CENTER,
    autoClose: 1000,
    closeOnClick: true,
    hideProgressBar: true,
  });
};
