import { api } from "api";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { toastError } from "./toastify";
import useLanguages from "./useLanguages";

const useRoomInfo = (id) => {
  const [room, setRoom] = useState({});
  const [loading, setLoading] = useState(false);
  const userName = useSelector((state) => state.auth.user.userName);
  const updateRoomStatus = useSelector((state) => state.room.updateRoomStatus);
  const { Texts } = useLanguages();
  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        const result = await api.getRoomInfo(id, userName);
        const member = result.data.members.find(
          (item) => item.userName !== userName
        );
        setRoom({
          ...result.data,
          memberName: member?.userName,
          memberAvatar: member?.avatar,
          memberBio: member?.bio,
        });
      } catch (error) {
        toastError(Texts.roomError, true);
      } finally {
        setLoading(false);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateRoomStatus, id]);

  return { room, loading };
};

export default useRoomInfo;
