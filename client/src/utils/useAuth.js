import { login, setRooms } from "actions";
import { api } from "api";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { toastError } from "./toastify";
import useLanguages from "./useLanguages";

const useAuth = () => {
  const [User, setUser] = useState({});
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const { Texts } = useLanguages();
  useEffect(() => {
    (async () => {
      try {
        const token = window.localStorage.getItem("__IE_PROJECT__");
        if (token) {
          setLoading(true);
          const result = await api.currentUser();
          setUser({ ...result.data, _id: result.data.id });
          dispatch(login({ ...result.data, _id: result.data.id }));
          dispatch(setRooms(result.data.rooms));
        }
      } catch (error) {
        window.localStorage.clear("__IE_PROJECT__");
        toastError(Texts.expiredSession, true);
      } finally {
        setLoading(false);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { User, loading };
};

export default useAuth;
