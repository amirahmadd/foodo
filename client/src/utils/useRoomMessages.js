import { api } from "api";
import { useEffect, useState } from "react";
import { toastError } from "./toastify";
import useLanguages from "./useLanguages";

const useRoomMessages = (roomId) => {
  const [Messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const { Texts } = useLanguages();
  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        const result = await api.getMessages(roomId);
        setMessages(result.data);
      } catch (error) {
        toastError(Texts.roomError, true);
      } finally {
        setLoading(false);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { Messages, loading };
};

export default useRoomMessages;
