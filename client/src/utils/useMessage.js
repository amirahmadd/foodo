import { api } from "api";
import { useEffect, useState } from "react";
import { toastError } from "./toastify";
import useLanguages from "./useLanguages";

const useMessage = (msgId, userId) => {
  const [Message, setMessage] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const { Texts } = useLanguages();
  useEffect(() => {
    (async () => {
      try {
        if (msgId !== true) {
          setLoading(true);
          const result = await api.getOneMessage(msgId, userId);
          setMessage({ ...result.data, sender: result.data.sender.$oid });
        }
      } catch (error) {
        setError(true);
        toastError(Texts.messageError, true);
      } finally {
        setLoading(false);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { Message, loading, error };
};

export default useMessage;
