import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { texts } from "Constants";
const useLanguages = () => {
  const [Texts, setTexts] = useState({});
  const language = useSelector((state) => state.room.language);
  useEffect(() => {
    if (texts[language]) {
      setTexts(texts[language]);
    } else {
      setTexts(texts.fa);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [language]);

  return { Texts };
};

export default useLanguages;
