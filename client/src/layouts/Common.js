import React from "react";

const Common = (props) => {
  return <div style={{ height: "100%" }}>{props.children}</div>;
};

export default Common;
