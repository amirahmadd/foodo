/* eslint-disable eqeqeq */
import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Close } from "@material-ui/icons";
import { useSelector } from "react-redux";
import { toastError, toastSuccess, useLanguages } from "utils";
import { api } from "api";
import {
  Modal,
  Paper,
  Fade,
  Grid,
  Typography,
  TextField,
  Button,
  IconButton,
  Switch,
} from "@material-ui/core";
import ProfileImage from "./ProfileImage";
import BackGroundSelector from "./BackGroundSelector";

const useStyle = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    border: "none",
    padding: theme.spacing(2, 4, 3),
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  },
  avatarContainer: {
    alignItems: "center",
  },
  btn: {
    color: theme.palette.success.contrastText,
    backgroundColor: theme.palette.success.main,
    marginTop: "10px",
    marginBottom: "2rem",
  },
  memberList: {
    maxHeight: "300px",
    overflowY: "scroll",
  },
}));
const SelfInfoModal = ({ open, handleClose, notifCheck, handleNotif }) => {
  const user = useSelector((state) => state.auth.user);
  const classes = useStyle();
  const { Texts } = useLanguages();
  const [bio, setbio] = useState("");
  useEffect(() => {
    setbio(user.bio);
  }, [user]);

  const handleEdit = async () => {
    try {
      await api.editUser({ bio, userName: user.userName });
      toastSuccess(Texts.successfullEdit);
      handleClose();
    } catch (error) {
      toastError(Texts.errorOnEdit, true);
    }
  };

  return user ? (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby={`simple-modal-title-self`}
      aria-describedby={`simple-modal-description-self`}
      closeAfterTransition
      BackdropProps={{
        timeout: 1000,
      }}
    >
      <Fade in={open}>
        <Paper className={classes.paper} elevation={4}>
          <span>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </span>
          <Grid container justify="center">
            <Grid container justify="center" item xs={12}>
              <ProfileImage src={user.avatar} />
            </Grid>
            <Grid container justify="center" item xs={12}>
              <Typography variant="body1">{user.userName}</Typography>
            </Grid>
            <Grid container justify="center" item xs={12}>
              <TextField
                value={bio}
                onChange={(e) => setbio(e.target.value)}
                label="bio"
              />
            </Grid>
            <Grid container justify="center" item xs={12}>
              <Button className={classes.btn} onClick={handleEdit}>
                {Texts.submitChanges}
              </Button>
            </Grid>
            <Grid item xs={6}>
              <Grid container justify="center" item xs={12}>
                <BackGroundSelector src={user.background} />
              </Grid>
              <Grid container justify="center" item xs={12}>
                {Texts.background}
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <Grid container justify="center" item xs={12}>
                <Switch
                  checked={notifCheck}
                  onChange={handleNotif}
                  name="notificationStatus"
                  inputProps={{ "aria-label": Texts.notification }}
                />
              </Grid>
              <Grid
                container
                justify="center"
                item
                xs={12}
                style={{ marginTop: "10px" }}
              >
                {Texts.notification}
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Fade>
    </Modal>
  ) : (
    <div>loading ...</div>
  );
};

export default SelfInfoModal;
