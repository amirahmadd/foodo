import React from "react";
import { Grid, Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import "./style.css";

const useStyle = makeStyles((theme) => ({
  root: {
    width: "100%",
    minHeight: "600px",
    position: "relative",
    overflow: "hidden",
  },
  imageContainer: {
    height: "600px",
  },
  textContainer: {
    position: "absolute",
    top: "30%",
    left: "10%",
    [theme.breakpoints.down("sm")]: {
      position: "static",
      margin: "2.5rem 0 -2.5rem 0",
    },
  },
  titleContainer: {
    // height: "400px",
    textAlign: "center",
    alignItems: "center",
    zIndex: "9",
  },
  title: {
    fontWeight: "bold",
    marginBottom: "3rem",
    color: theme.palette.primary.contrastText,
    [theme.breakpoints.down("sm")]: {
      marginTop:"1rem",
      color: theme.palette.primary.dark,
    },
  },
  subtitle: {
    fontWeight: "bold",
    color: theme.palette.primary.contrastText,
    [theme.breakpoints.down("sm")]: {
      color: theme.palette.primary.dark,
    },
  },
  backgroundShape: {
    width: "750px",
    height: "700px",
    position: "absolute",
    left: "-100px",
    top: "-30%",
    borderRadius: "70% 30% 30% 70% / 60% 40% 60% 40%",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
}));
const Banner = (props) => {
  const classes = useStyle();
  return (
    <Paper className={classes.root}>
      <Grid container justify="center">
        <Grid item xs={12} md={6} className={classes.titleContainer}>
          <div className={classes.textContainer}>
            <Typography variant="h3" className={classes.title} color="primary">
              {props.title}
            </Typography>
            <Typography
              variant="subtitle1"
              className={classes.subtitle}
              color="primary"
            >
              {props.subtitle}
            </Typography>
            <div>
              <Typography
                variant="subtitle1"
                className={classes.subtitle}
                color="primary"
              >
                {props.subtitle2}
              </Typography>
            </div>
          </div>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          className={classes.imageContainer}
          style={{
            background: `url(${
              process.env.PUBLIC_URL + props.backgroundImage
            }) no-repeat center `,
            backgroundAttachment: "inherit",
            backgroundSize: "contain",
          }}
        >
          <span className={classes.backgroundShape} id="backgroundShape" />
        </Grid>
      </Grid>
    </Paper>
  );
};

export default Banner;
