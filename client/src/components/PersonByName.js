import React, { useState } from "react";
import {
  Avatar,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import SelfInfoModal from "./SelfInfoModal";
import { useSelector } from "react-redux";
import { useLanguages } from "utils";

const PersonByName = ({ userName, handleNotif, notifCheck, avatar }) => {
  const [open, setOpen] = useState(false);
  const socketStatus = useSelector((state) => state.room.socketStatus);
  const { Texts } = useLanguages();
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <ListItem>
        <ListItemIcon>
          <Avatar
            src={
              avatar
                ? process.env.AVATAR_URL
                  ? process.env.AVATAR_URL
                  : process.env.REACT_APP_AVATAR_URL + "/" + avatar
                : ""
            }
          />
        </ListItemIcon>
        <ListItemText
          onClick={handleOpen}
          secondary={
            socketStatus ? (
              Texts.online
            ) : (
              <span style={{ color: "red" }}>{Texts.offline}</span>
            )
          }
          primary={userName}
          style={{ cursor: "pointer" }}
        />
      </ListItem>
      <SelfInfoModal
        open={open}
        handleClose={handleClose}
        self
        handleNotif={handleNotif}
        notifCheck={notifCheck}
        // id={roomId}
        // room={room}
      />
    </>
  );
};

export default PersonByName;
