/* eslint-disable eqeqeq */
import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useLanguages, useRoomInfo } from "utils";
import { Skeleton } from "@material-ui/lab";
import { useSelector } from "react-redux";
import { Group as GroupIcon } from "@material-ui/icons";
import {
  Avatar,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import InfoModal from "./InfoModal";

const useStyle = makeStyles((theme) => ({
  link: {
    textDecoration: "none",
    color: theme.palette.text.primary,
  },
  header: {
    color: theme.palette.primary.contrastText,
    cursor: "pointer",
  },
  messageText: {
    color: theme.palette.primary.contrastText,
  },
  seen: {
    color: theme.palette.primary.contrastText,
  },
  notSeen: {
    color: theme.palette.primary.light,
  },
}));
const RoomHeader = ({ roomId, button }) => {
  const classes = useStyle();
  const [open, setOpen] = useState(false);
  const status = useSelector((state) => state.room.isTyping);
  const { room, loading } = useRoomInfo(roomId);
  const { Texts } = useLanguages();
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return !loading || room != {} ? (
    <ListItem>
      <ListItemIcon>
        {room.title == null ? (
          <Avatar
            src={
              room.memberAvatar
                ? process.env.AVATAR_URL
                  ? process.env.AVATAR_URL
                  : process.env.REACT_APP_AVATAR_URL + "/" + room.memberAvatar
                : ""
            }
          ></Avatar>
        ) : (
          <Avatar
            src={
              room.avatar
                ? process.env.AVATAR_URL
                  ? process.env.AVATAR_URL
                  : process.env.REACT_APP_AVATAR_URL + "/" + room.avatar
                : ""
            }
          >
            <GroupIcon />
          </Avatar>
        )}
      </ListItemIcon>

      <ListItemText
        onClick={handleOpen}
        className={classes.header}
        secondary={
          status ? (
            <>
              {Texts.isTyping}
              <span id="loadingDot"></span>
            </>
          ) : (
            ""
          )
        }
        primary={room.title == null ? room.memberName : room.title}
      />
      <InfoModal
        open={open}
        handleClose={handleClose}
        id={roomId}
        room={room}
      />
    </ListItem>
  ) : (
    <ListItem>
      <ListItemIcon>
        <Skeleton animation="wave" variant="circle" width={40} height={40} />
      </ListItemIcon>
      <ListItemText
        secondary={<Skeleton animation="wave" height={10} width="80%" />}
        primary={<Skeleton animation="wave" height={10} width="80%" />}
      />
    </ListItem>
  );
};

export default RoomHeader;
