import React from "react";
import { Avatar, Grid, IconButton } from "@material-ui/core";
import { Close, Group } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import { toastError, toastSuccess, useLanguages } from "utils";
import { api } from "api";
import { useParams } from "react-router-dom";
const useStyle = makeStyles((theme) => ({
  avatar: {
    cursor: "pointer",
    width: "4rem",
    height: "4rem",
  },
  close: {
    color: theme.palette.secondary.main,
  },
}));

const GroupImage = ({ src }) => {
  const classes = useStyle();
  const { Texts } = useLanguages();
  const { roomId } = useParams();
  const uploadAvatar = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      try {
        await api.editGroupAvatar(e.target.files[0], roomId);
        toastSuccess(Texts.successfullEdit);
      } catch (error) {
        toastError(Texts.errorOnEdit, true);
      }
    }
  };

  const removeAvatar = async () => {
    try {
      await api.removeGroupAvatar(roomId);
      toastSuccess(Texts.successfullEdit);
    } catch (error) {
      toastError(Texts.errorOnEdit, true);
    }
  };
  return (
    <Grid container justify="center">
      <Grid container item xs={12} justify="center">
        <input
          name="Avatar"
          id="attach-Avatar"
          type="file"
          accept="image/*"
          hidden
          onChange={(e) => uploadAvatar(e)}
        />
        <label htmlFor="attach-Avatar">
          <Avatar
            className={classes.avatar}
            src={
              src
                ? process.env.AVATAR_URL
                  ? process.env.AVATAR_URL
                  : process.env.REACT_APP_AVATAR_URL + "/" + src
                : ""
            }
          >
            {!src && <Group fontSize="large" />}
          </Avatar>
        </label>
      </Grid>
      {src && (
        <Grid container item xs={12} justify="center">
          <IconButton onClick={removeAvatar}>
            <Close className={classes.close} />
          </IconButton>
        </Grid>
      )}
    </Grid>
  );
};

export default GroupImage;
