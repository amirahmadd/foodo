import React from "react";
import Spinner from "./Spinner";

const WithLoading = (Component) => {
  return function WihLoadingComponent({ isLoading, ...props }) {
    if (Component) return <Component {...props} />;
    return <Spinner />;
  };
};

export default WithLoading;
