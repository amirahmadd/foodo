import React from "react";
import { Avatar, Grid, IconButton } from "@material-ui/core";
import { Close, Person } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import { toastError, useLanguages } from "utils";
import { api } from "api";
import { useDispatch } from "react-redux";
import { login } from "actions";
const useStyle = makeStyles((theme) => ({
  avatar: {
    cursor: "pointer",
    width: "4rem",
    height: "4rem",
  },
  close: {
    color: theme.palette.secondary.main,
  },
}));

const ProfileImage = ({ src }) => {
  const classes = useStyle();
  const { Texts } = useLanguages();
  const dispatch = useDispatch();
  const uploadUserAvatar = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      try {
        const result = await api.editUserAvatar(e.target.files[0]);
        dispatch(login({ ...result.data, _id: result.data.id }));
      } catch (error) {
        toastError(Texts.imageError, true);
      }
    }
  };

  const removeAvatar = async () => {
    try {
      const result = await api.removeUserAvatar();
      dispatch(login({ ...result.data, _id: result.data.id }));
    } catch (error) {
      toastError(Texts.avatarDeleteError, true);
    }
  };
  return (
    <Grid container justify="center">
      <Grid container item xs={12} justify="center">
        <input
          name="Avatar"
          id="attach-Avatar"
          type="file"
          accept="image/*"
          hidden
          onChange={(e) => uploadUserAvatar(e)}
        />
        <label htmlFor="attach-Avatar">
          <Avatar
            className={classes.avatar}
            src={
              src
                ? process.env.AVATAR_URL
                  ? process.env.AVATAR_URL
                  : process.env.REACT_APP_AVATAR_URL + "/" + src
                : ""
            }
          >
            {!src && <Person fontSize="large" />}
          </Avatar>
        </label>
      </Grid>
      {src && (
        <Grid container item xs={12} justify="center">
          <IconButton onClick={removeAvatar}>
            <Close className={classes.close} />
          </IconButton>
        </Grid>
      )}
    </Grid>
  );
};

export default ProfileImage;
