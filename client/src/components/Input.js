/* eslint-disable eqeqeq */
import React, { useEffect, useState } from "react";
import { Grid, IconButton, TextField } from "@material-ui/core";
import { Send } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import { useLanguages } from "utils";
const useStyle = makeStyles((theme) => ({
  sendIcon: {
    transform:
      window.localStorage.getItem("__IE_PROJECT_lang__") == "fa" ||
      !window.localStorage.getItem("__IE_PROJECT_lang__")
        ? "rotate(180deg)"
        : "",
    color: theme.palette.success.main,
    // position: "relative",
    // top: "4px",
  },
  input: {
    borderRadius: "10px",
    position: "relative",
    top: "3px",
  },
}));
const Input = (props) => {
  const { Texts } = useLanguages();
  const [text, setText] = useState("");
  const [timer, setTimer] = useState("");
  const classes = useStyle();
  const handleInput = (e) => {
    setText(e.target.value);
    props.handleTyping();
    setTimer(
      setTimeout(() => {
        handleEndTyping();
        setTimer("");
      }, 5000)
    );
  };
  const handleEndTyping = () => {
    clearTimeout(timer);
    setTimer("");
    props.handleEndTyping();
  };
  const handleSend = (e) => {
    e.preventDefault();
    props.handleSend(text);
    handleEndTyping();
    setText("");
  };
  useEffect(() => {
    return () => {
      clearTimeout(timer);
      setTimer("");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <form onSubmit={handleSend}>
      <Grid container item xs={12}>
        <Grid item xs={10} sm={11}>
          <TextField
            variant="filled"
            fullWidth
            placeholder={Texts.messageText}
            value={text}
            onChange={handleInput}
            className={classes.input}
            onBlur={handleEndTyping}
          />
        </Grid>
        <Grid item xs={1} container justify="center">
          <IconButton type="submit" onClick={handleSend}>
            <Send fontSize="large" className={classes.sendIcon} />
          </IconButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default Input;
