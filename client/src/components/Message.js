/* eslint-disable eqeqeq */
import React, { useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Paper, Typography } from "@material-ui/core";
import { useSelector } from "react-redux";
import { useMessage } from "utils";
import { Skeleton } from "@material-ui/lab";

const useStyle = makeStyles((theme) => ({
  root: {},
  textContainer: {
    wordWrap: "break-word",
  },
  msg: {
    display: "inline-block",
    padding: "8px",
    maxWidth: "100%",
    borderRadius: "5px",
    color: "#ffffff",
  },
  rtlText: {
    textAlign: "left",
  },
}));
const Message = ({ message }) => {
  const ref = useRef(null);
  const classes = useStyle();
  const userId = useSelector((state) => state.auth.user._id);
  const userName = useSelector((state) => state.auth.user.userName);
  const { Message, loading, error } = useMessage(message, userId);

  useEffect(() => {
    let scrollTimer = setTimeout(() => {
      if (ref.current)
        ref.current.scrollIntoView({
          block: "end",
          behavior: "smooth",
        });
    }, 300);
    return () => clearTimeout(scrollTimer);
  }, [loading]);
  return !loading ? (
    !error ? (
      <Grid
        container
        justify={Message.sender === userId ? "flex-start" : "flex-end"}
        className={classes.root}
        spacing={3}
      >
        <Grid item xs={8}>
          <div
            className={classes.textContainer}
            style={{ textAlign: Message.sender !== userId && "end" }}
          >
            <Paper
              ref={ref}
              elevation={5}
              className={classes.msg}
              style={{
                backgroundColor:
                  Message.sender !== userId ? "#21a6ff" : "#30a844",
              }}
            >
              <Typography variant="caption">
                {userName != Message.userName && Message.userName}
              </Typography>
              <Typography
                className={Message.sender !== userId ? classes.rtlText : ""}
              >
                {Message.text}
              </Typography>
              <Typography variant="caption" className={classes.secondaryText}>
                {Message.time}
                {/* {Message?.isSeen ? (
                  <span className={classes.seen}>
                    {userId == Message?.sender ? "✓✓" : ""}
                  </span>
                ) : Message?.text ? (
                  <span className={classes.notSeen}>
                    {userId == Message?.sender ? "✓" : ""}
                  </span>
                ) : (
                  ""
                )} */}
              </Typography>
            </Paper>
          </div>
        </Grid>
      </Grid>
    ) : (
      ""
    )
  ) : (
    <div style={{ margin: "10px" }}>
      <Skeleton variant="rect" width={80} height={20} animation="wave" />
    </div>
  );
};

export default Message;
