import React from "react";
import { Avatar, Grid, IconButton } from "@material-ui/core";
import { Close, Image } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import { toastError, useLanguages } from "utils";
import { api } from "api";
import { useDispatch } from "react-redux";
import { login } from "actions";
const useStyle = makeStyles((theme) => ({
  avatar: {
    cursor: "pointer",
    width: "3rem",
    height: "3rem",
  },
  close: {
    color: theme.palette.secondary.main,
  },
}));

const BackGroundSelector = ({ src }) => {
  const classes = useStyle();
  const { Texts } = useLanguages();
  const dispatch = useDispatch();

  const uploadBackground = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      try {
        const result = await api.editUserBackground(e.target.files[0]);
        dispatch(login({ ...result.data, _id: result.data.id }));
      } catch (error) {
        toastError(Texts.imageError, true);
      }
    }
  };

  const removeBackground = async () => {
    try {
      const result = await api.removeUserBackground();
      dispatch(login({ ...result.data, _id: result.data.id }));
    } catch (error) {
      toastError(Texts.avatarDeleteError, true);
    }
  };
  return (
    <Grid container justify="center">
      <Grid container item xs={src ? 12 : 8} md={src ? 12 : 4} justify="center">
        <input
          name="background"
          id="attach-background"
          type="file"
          accept="image/*"
          hidden
          onChange={(e) => uploadBackground(e)}
        />
        <label htmlFor="attach-background">
          <Avatar
            className={classes.avatar}
            src={
              src
                ? process.env.BG_URL
                  ? process.env.BG_URL
                  : process.env.REACT_APP_BG_URL + "/" + src
                : ""
            }
          >
            {!src && <Image />}
          </Avatar>
        </label>
      </Grid>
      {src && (
        <Grid container item xs={3} justify="center">
          <IconButton onClick={removeBackground}>
            <Close className={classes.close} />
          </IconButton>
        </Grid>
      )}
    </Grid>
  );
};

export default BackGroundSelector;
