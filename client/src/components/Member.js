/* eslint-disable eqeqeq */
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Skeleton } from "@material-ui/lab";
import {
  // Group as GroupIcon,
  Close,
} from "@material-ui/icons";
import {
  Avatar,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton,
} from "@material-ui/core";
import { useLanguages } from "utils";
// import InfoModal from "./InfoModal";

const useStyle = makeStyles((theme) => ({
  link: {
    textDecoration: "none",
    color: theme.palette.text.primary,
  },
  header: {
    color: theme.palette.success.contrastText,
  },
  close: {
    color: theme.palette.secondary.main,
  },
}));
const Member = ({ room, button, admin, hasAccess, handleClick }) => {
  const classes = useStyle();
  const { Texts } = useLanguages();
  //   const [open, setOpen] = useState(false);
  //   const status = useSelector((state) => state.room.isTyping);
  //   const handleOpen = () => {
  //     setOpen(true);
  //   };
  //   const handleClose = () => {
  //     setOpen(false);
  //   };
  return room != {} ? (
    <ListItem button={button}>
      <ListItemIcon>
        <Avatar
          src={
            room.avatar
              ? process.env.AVATAR_URL
                ? process.env.AVATAR_URL
                : process.env.REACT_APP_AVATAR_URL + "/" + room.avatar
              : ""
          }
        />
      </ListItemIcon>
      <ListItemText
        className={classes.header}
        secondary={admin ? Texts.admin : ""}
        primary={room.userName}
      />
      {hasAccess && !admin && (
        <IconButton onClick={handleClick}>
          <Close className={classes.close} />
        </IconButton>
      )}

      {/* <InfoModal open={open} handleClose={handleClose} room={room} /> */}
    </ListItem>
  ) : (
    <ListItem>
      <ListItemIcon>
        <Skeleton animation="wave" variant="circle" width={40} height={40} />
      </ListItemIcon>
      <ListItemText
        secondary={<Skeleton animation="wave" height={10} width="80%" />}
        primary={<Skeleton animation="wave" height={10} width="80%" />}
      />
    </ListItem>
  );
};

export default Member;
