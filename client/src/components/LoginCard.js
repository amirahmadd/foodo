/* eslint-disable eqeqeq */
import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Grid, Paper, TextField, Typography } from "@material-ui/core";
import { api } from "api";
import { toastError, toastSuccess, useLanguages } from "utils";
import { useDispatch } from "react-redux";
import { login } from "actions";
import { setRooms } from "actions/roomActions";
import LangSelect from "./LangSelect";

const useStyle = makeStyles((theme) => ({
  card: {
    margin: "5px",
    transition: "all 0.3s cubic-bezier(.25,.8,.25,1)",
    boxShadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
    opacity: "80%",
    borderRadius: "15px",
    "&:hover": {
      opacity: "100%",
      boxShadow: "0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)",
      marginTop: "-2px",
    },
    height: "370px",
    padding: "1rem",
  },
  title: {
    color: "#FFFFFF",
    textAlign: "left",
    position: "absolute",
    top: "10px",
    fontWeight: "bold",
    fontSize: "2.22rem",
  },
  CardActionArea: {
    height: "100%",
  },
  link: {
    textDecoration: "none",
  },
}));
const API_URL = process.env.API_URL
  ? process.env.API_URL
  : process.env.REACT_APP_API_URL ?? "http://localhost:5000";
const LoginCard = () => {
  const classes = useStyle();
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setloading] = useState(false);
  const dispatch = useDispatch();
  const { Texts } = useLanguages();
  const handleLogin = async (e) => {
    e.preventDefault();
    if (!userName || !password) {
      toastError(Texts.enterYourInfo, true);
      return;
    }
    try {
      setloading(true);
      const result = await api.login({ userName, password });
      localStorage.setItem("__IE_PROJECT__", result.data.token);
      const user = result.data.user;
      dispatch(login({ ...user, _id: user.id }));
      dispatch(setRooms(user.rooms));
      toastSuccess(Texts.welcome);
    } catch (error) {
      toastError(Texts.wrongCred, true);
    } finally {
      setloading(false);
    }
  };

  return (
    <>
      <Paper className={classes.card}>
        <form onSubmit={handleLogin}>
          <Grid container justify="center" spacing={6}>
            <Grid item xs={12}>
              <Typography>{Texts.enter}</Typography>
            </Grid>
            <Grid item xs={11}>
              <TextField
                value={userName}
                onChange={(e) => setUserName(e.target.value)}
                fullWidth
                variant="outlined"
                label={Texts.userName}
                dir="ltr"
                name="user"
              />
            </Grid>
            <Grid item xs={11}>
              <TextField
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                fullWidth
                variant="outlined"
                label={Texts.password}
                dir="ltr"
                type="password"
                name="pas"
              />
            </Grid>
            <Grid item xs={11}>
              <Button
                onClick={handleLogin}
                variant="contained"
                fullWidth
                color="primary"
                disabled={loading}
                type="submit"
              >
                {Texts.enterBtn}
              </Button>
              <a
                href={`${API_URL}/signup${
                  window.localStorage.getItem("__IE_PROJECT_lang__") == "fa" ||
                  window.localStorage.getItem("__IE_PROJECT_lang__") == null
                    ? "/fa"
                    : "/en"
                }`}
                className={classes.link}
              >
                <Button
                  variant="text"
                  fullWidth
                  color="primary"
                  disabled={loading}
                >
                  {Texts.signUp}
                </Button>
              </a>
            </Grid>
          </Grid>
        </form>
      </Paper>
      <LangSelect />
    </>
  );
};

export default LoginCard;
