/* eslint-disable eqeqeq */
import React from "react";
import { Link, useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { useLanguages, useRoomInfo } from "utils";
import { Skeleton } from "@material-ui/lab";
import { useSelector } from "react-redux";
import { Group as GroupIcon } from "@material-ui/icons";
import {
  Avatar,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@material-ui/core";

const useStyle = makeStyles((theme) => ({
  link: {
    textDecoration: "none",
    color: theme.palette.text.primary,
  },
  header: {
    color: theme.palette.primary.contrastText,
  },
  messageText: {
    color: theme.palette.primary.contrastText,
    display: "block",
  },
  seen: {
    color: theme.palette.primary.contrastText,
  },
  notSeen: {
    // color: theme.palette.primary.light,
    color: "#ffffff",
  },
}));
const Group = ({ roomId, button }) => {
  const classes = useStyle();
  const params = useParams();
  const userId = useSelector((state) => state.auth.user._id);
  const { room, loading } = useRoomInfo(roomId);
  const { Texts } = useLanguages();
  return !loading || room != {} ? (
    room.title != null ? (
      <Link to={`/${button ? roomId : ""}`} className={classes.link}>
        <ListItem button={button} selected={params.roomId === roomId}>
          <ListItemIcon>
            <Avatar
              src={
                room.avatar
                  ? process.env.AVATAR_URL
                    ? process.env.AVATAR_URL
                    : process.env.REACT_APP_AVATAR_URL + "/" + room.avatar
                  : ""
              }
            >
              <GroupIcon />
            </Avatar>
          </ListItemIcon>
          <ListItemText
            secondary={
              <span>
                <Typography
                  component="span"
                  noWrap
                  className={classes.messageText}
                >
                  {room.lastMessage?.text}
                </Typography>
                <span>
                  {room.lastMessage?.time}
                  {room.lastMessage?.isSeen ? (
                    <span className={classes.seen}>
                      {userId == room.lastMessage?.sender ? "✓✓" : ""}
                    </span>
                  ) : room.lastMessage?.text ? (
                    <span className={classes.notSeen}>
                      {userId == room.lastMessage?.sender
                        ? "✓"
                        : Texts.newMessage}
                    </span>
                  ) : (
                    ""
                  )}
                </span>
              </span>
            }
            primary={room.title}
          />
        </ListItem>
      </Link>
    ) : null
  ) : (
    <ListItem button={button} selected={params.roomId === roomId}>
      <ListItemIcon>
        <Skeleton animation="wave" variant="circle" width={40} height={40} />
      </ListItemIcon>
      <ListItemText
        secondary={<Skeleton animation="wave" height={10} width="80%" />}
        primary={<Skeleton animation="wave" height={10} width="80%" />}
      />
    </ListItem>
  );
};

export default Group;
