/* eslint-disable eqeqeq */
import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Close, Group as GroupIcon, Person } from "@material-ui/icons";
import { useSelector } from "react-redux";
import {
  Modal,
  Paper,
  Fade,
  Grid,
  Avatar,
  Typography,
  TextField,
  Button,
  IconButton,
} from "@material-ui/core";
import Member from "./Member";
import { toastError, toastSuccess, toastWarning, useLanguages } from "utils";
import { api } from "api";
import { useHistory } from "react-router-dom";
import GroupImage from "./GroupImage";

const useStyle = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    border: "none",
    padding: theme.spacing(2, 4, 3),
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
    paddingBottom: "2rem",
  },
  avatarContainer: {
    alignItems: "center",
  },
  avatar: {
    width: "4rem",
    height: "4rem",
  },
  btn: {
    color: theme.palette.success.contrastText,
    backgroundColor: theme.palette.success.main,
    marginTop: "10px",
  },
  memberList: {
    maxHeight: "300px",
    overflowY: "scroll",
  },
  exitBtn: {
    marginTop: "1rem",
  },
}));
const InfoModal = ({ open, handleClose, room, id }) => {
  const user = useSelector((state) => state.auth.user.userName);
  const classes = useStyle();
  const [title, setTitle] = useState("");
  const [bio, setbio] = useState("");
  const [searchPhase, setSearchPhase] = useState("");
  const history = useHistory();
  const { Texts } = useLanguages();
  useEffect(() => {
    setTitle(room.title);
    setbio(room.bio);
  }, [room]);

  const handleEdit = async () => {
    try {
      if (!title) {
        toastError(Texts.enterGroupTitleEdit, true);
        return;
      }
      await api.editGroup({ title, bio, roomId: id });
      toastSuccess(Texts.successfullEdit);
      handleClose();
    } catch (error) {
      toastError(Texts.errorOnEdit, true);
    }
  };
  const handleAddMember = async () => {
    try {
      const rUser = room.members.find((item) => item.userName == searchPhase);
      if (!searchPhase) {
        toastError(Texts.enterUserName, true);
        return;
      } else if (rUser) {
        toastError(Texts.duplicateUserName, true);
        return;
      }
      await api.addMember({ userName: searchPhase, roomId: id });
      toastSuccess();
    } catch (error) {
      if (error?.response?.status == 401) {
        toastError(Texts.newMemberAdded, true);
      } else {
        toastError(Texts.errorOnEdit, true);
      }
    }
  };
  const handleRemoveUser = async (userName) => {
    try {
      await api.removeMember({ userName, roomId: id });
      toastWarning(Texts.removedUser);
    } catch (error) {
      toastError(Texts.errorOnEdit, true);
    }
  };

  const handleLeaveGroup = async () => {
    try {
      await api.leaveChat({ userName: user, roomId: id });
      toastWarning(Texts.successFullLeft);
      history.push("/");
      handleClose();
    } catch (error) {
      toastError(Texts.errorOnEdit, true);
    }
  };

  return user && room != {} && room.members ? (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby={`simple-modal-title${room.title || room.memberName}`}
      aria-describedby={`simple-modal-description${
        room.title || room.memberName
      }`}
      closeAfterTransition
      BackdropProps={{
        timeout: 1000,
      }}
    >
      <Fade in={open}>
        <Paper className={classes.paper}>
          <span>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </span>
          <Grid container justify="center">
            <Grid container justify="center" item xs={12}>
              {room.title == null ? (
                <Avatar
                  className={classes.avatar}
                  src={
                    room.memberAvatar
                      ? process.env.AVATAR_URL
                        ? process.env.AVATAR_URL
                        : process.env.REACT_APP_AVATAR_URL +
                          "/" +
                          room.memberAvatar
                      : ""
                  }
                >
                  <Person fontSize="large" />
                </Avatar>
              ) : user == room?.members[0]?.userName ? (
                <GroupImage src={room.avatar} />
              ) : (
                <Avatar
                  className={classes.avatar}
                  src={
                    room.avatar
                      ? process.env.AVATAR_URL
                        ? process.env.AVATAR_URL
                        : process.env.REACT_APP_AVATAR_URL + "/" + room.avatar
                      : ""
                  }
                >
                  <GroupIcon fontSize="large" />
                </Avatar>
              )}
            </Grid>
            <Grid container justify="center" item xs={12}>
              <Typography variant="body1">
                {room.title == null ? (
                  room.memberName
                ) : user == room?.members[0]?.userName ? (
                  <TextField
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    label={Texts.groupTitle}
                    // inputProps={{ style: { textAlign: "center" } }}
                  />
                ) : (
                  room.title
                )}
              </Typography>
            </Grid>
            <Grid container justify="center" item xs={12}>
              <Typography variant="caption">
                {room.title == null ? (
                  room.memberBio
                ) : user == room?.members[0]?.userName ? (
                  <TextField
                    value={bio}
                    onChange={(e) => setbio(e.target.value)}
                    label="bio"
                    // inputProps={{ style: { textAlign: "center" } }}
                  />
                ) : (
                  room.bio
                )}
              </Typography>
            </Grid>
            {user == room?.members[0]?.userName && room.title != null ? (
              <Grid container justify="center" item xs={12}>
                <Button className={classes.btn} onClick={handleEdit}>
                  {Texts.submitChanges}
                </Button>
              </Grid>
            ) : (
              ""
            )}
            {user == room?.members[0]?.userName && room.title != null ? (
              <Grid container justify="center">
                <Grid item xs={10} md={7}>
                  <TextField
                    value={searchPhase}
                    onChange={(e) => setSearchPhase(e.target.value)}
                    label={Texts.userName}
                    fullWidth
                    // inputProps={{ style: { textAlign: "center" } }}
                  />
                </Grid>
                <Grid item xs={10} md={3}>
                  <Button
                    fullWidth
                    className={classes.btn}
                    onClick={handleAddMember}
                  >
                    {Texts.add}
                  </Button>
                </Grid>
              </Grid>
            ) : (
              ""
            )}
          </Grid>
          {room.title != null && (
            <Grid
              item
              xs={12}
              container
              justify="center"
              className={classes.memberList}
            >
              {room.members.map((item) => (
                <Member
                  key={item.userName}
                  room={item}
                  handleClick={() => handleRemoveUser(item.userName)}
                  admin={item?.userName == room?.members[0]?.userName}
                  hasAccess={user == room?.members[0]?.userName}
                />
              ))}
            </Grid>
          )}
          <Grid item xs={12} container justify="center">
            <Button
              onClick={handleLeaveGroup}
              variant="contained"
              className={classes.exitBtn}
              color="secondary"
            >
              {Texts.exit}
            </Button>
          </Grid>
        </Paper>
      </Fade>
    </Modal>
  ) : (
    <div>loading ...</div>
  );
};

export default InfoModal;
