import React from "react";
import { FormControl, Select, MenuItem } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { setLanguage } from "actions";
import { languages } from "Constants";
const LangSelect = ({ light }) => {
  const dispatch = useDispatch();
  const lang = useSelector((state) => state.room.language);
  const handleChangeLanguage = (e) => {
    dispatch(setLanguage(e.target.value));
    document.body.dir = e.target.value === "fa" ? "rtl" : "ltr";
    document.title = e.target.value === "fa" ? "پیامرسان آرک" : "ARK Messenger";
    window.localStorage.setItem("__IE_PROJECT_lang__", e.target.value);
    window.location.reload();
  };
  return (
    <FormControl>
      <Select
        value={lang}
        onChange={handleChangeLanguage}
        style={{ color: light ? "#FFFFFF" : "#000000" }}
        color="primary"
        inputProps={{ "aria-label": "Without label" }}
      >
        {languages.map((item) => (
          <MenuItem key={item.value} value={item.value}>
            {item.title}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default LangSelect;
