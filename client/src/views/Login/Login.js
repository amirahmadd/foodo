import React from "react";
import { useStyle } from "./style";
import { Grid } from "@material-ui/core";
import { LoginCard } from "components";

const Login = () => {
  const classes = useStyle();
  return (
    <div className={classes.root}>
      <Grid container justify="center" className={classes.container}>
        <Grid item xs={12} sm={5} md={3} className={classes.boxContainer}>
          <LoginCard />
        </Grid>
      </Grid>
    </div>
  );
};

export default Login;
