import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles((theme) => ({
  root: {
    height: "100%",
    // background:
    //   "url(https://picsum.photos/1000/600/?blur=4) no-repeat center center fixed",
    // backgroundSize: "cover",
    overflow: "hidden",
  },
  boxContainer: {
    textAlign: "center",
    alignItems: "center",
  },
  container: {
    position: "relative",
    top: "10%",
  },
}));

export default useStyle;
