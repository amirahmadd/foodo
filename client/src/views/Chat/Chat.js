import React, { useEffect } from "react";
import useStyle from "./style";
import { Input, Message } from "components";
import { Grid, Paper } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { setCurrentRoom, setMessages } from "actions/roomActions";
import { useParams } from "react-router-dom";
const Chat = () => {
  const classes = useStyle();
  const socket = useSelector((state) => state.room.socket);
  const messages = useSelector((state) => state.room.messages);
  const room = useSelector((state) => state.room.currentRoom);
  const dispatch = useDispatch();
  const params = useParams();
  const userId = useSelector((state) => state.auth.user._id);

  useEffect(() => {
    if (socket) {
      dispatch(setCurrentRoom(params.roomId));
      socket.emit("joinChat", params.roomId);
    }
    return () => {
      if (socket) {
        socket.emit("leaveChat", params.roomId);
        dispatch(setCurrentRoom({}));
        dispatch(setMessages([]));
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params, socket]);

  const checkTime = (i) => {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  };
  const handleSend = (msg) => {
    let now = new Date();
    const data = {
      text: msg,
      time: `${checkTime(now.getHours())}:${checkTime(now.getMinutes())}`,
      sender: userId,
    };
    socket.emit("message", { msg: data, room });
  };

  const handleTyping = () => {
    socket.emit("istyping", { userId, room });
  };
  const handleEndTyping = () => {
    socket.emit("endtyping", { userId, room });
  };

  return (
    <Grid container className={classes.root} justify="center">
      <div
        style={{
          minHeight: "75vh",
          maxHeight: "75vh",
          width: "100%",
          overflowY: "scroll",
          overflowX: "hidden",
        }}
      >
        {messages.map((item) => (
          <Message message={item} key={item} />
        ))}
      </div>
      <div style={{ height: "3vh", width: "100%" }}>
        <Paper className={classes.inputBox} elevation={5}>
          <Input
            handleTyping={handleTyping}
            handleEndTyping={handleEndTyping}
            handleSend={(text) => handleSend(text)}
          />
        </Paper>
      </div>
    </Grid>
  );
};

export default Chat;
