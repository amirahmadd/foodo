import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles((theme) => ({
  root: {
    padding: "1rem",
  },
  
  inputBox: {
    borderRadius: "10px",
    width:"100%",
  },
}));

export default useStyle;
