import React, { lazy, useEffect } from "react";
import { RouteWithLayout, Spinner, WithLoading } from "components";
import { useSelector } from "react-redux";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import useAuth from "utils/useAuth";

// lazy load layouts
const Common = lazy(() => import("layouts/Common"));
const ChatRoom = lazy(() => import("layouts/ChatRoom"));

// lazy load views
const LoginComponent = lazy(() => import("views/Login/Login"));
const ChatComponent = lazy(() => import("views/Chat/Chat"));

// handle loading components
const login = WithLoading(LoginComponent);
const Chat = WithLoading(ChatComponent);
const Test = () => <div></div>;
const Routes = () => {
  const user = useSelector((state) => state.auth.user);
  const { loading } = useAuth();
  const userRoutes = {
    loginedUser: [
      { path: "/", component: Test, Layout: ChatRoom, exact: true },
      { path: "/:roomId", component: Chat, Layout: ChatRoom, exact: true },
    ],
    default: [
      { path: "/login", component: login, Layout: Common, exact: true },
      { path: "/", component: login, Layout: Common },
    ],
  };
  useEffect(() => {
    if (user?.background) {
      console.log("test");
      const bgUrl = process.env.BG_URL
        ? process.env.BG_URL
        : process.env.REACT_APP_BG_URL + "/" + user.background;
      document.body.style.background = `url('${bgUrl}') no-repeat center center fixed`;
      document.body.style.backgroundSize = "cover";
    } else {
      document.body.style.background = `url('https://picsum.photos/1000/600/?blur=4') no-repeat center center fixed`;
      document.body.style.backgroundSize = "cover";
    }
  }, [user]);
  return (
    <div style={{ height: "100%" }}>
      <Router>
        <Switch>
          {!loading ? (
            user ? (
              userRoutes["loginedUser"].map((item) => (
                <RouteWithLayout
                  key={item.path}
                  path={item.path}
                  exact={item.exact}
                  Component={item.component}
                  Layout={item.Layout}
                />
              ))
            ) : (
              userRoutes["default"].map((item) => (
                <RouteWithLayout
                  key={item.path}
                  path={item.path}
                  exact={item.exact}
                  Component={item.component}
                  Layout={item.Layout}
                />
              ))
            )
          ) : (
            <Spinner />
          )}
        </Switch>
      </Router>
    </div>
  );
};

export default Routes;
